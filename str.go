package main

import (
	"os"
	"bufio"
	"io/ioutil"
)

type Str struct {
	text string
}

// Метод для загрузки исходной строки из файла
func (s *Str) Load(path string) {
	strFile, err := os.Open(path)
	if err != nil {
		panic("Невозможно прочитать файл со строкой")
	}
	defer strFile.Close()

	reader := bufio.NewReader(strFile)
	buf, _ := ioutil.ReadAll(reader)
	s.text = string(buf)
}
