package main

import (
	"flag"
)

func main() {
	// Путь до файла с подстроками
	substrPath := flag.String("substr", "str.txt", "Путь до файла c подстроками")

	// Путь до файла с исходной строкой
	strPath := flag.String("str", "substr.txt", "Путь до файла со строкой")

	flag.Parse()

	str := Str{}
	str.Load(*strPath)

	substrList := SubstrList{}
	substrList.Load(*substrPath, str)

	res := Result{}

	res.combine(substrList, str)

	res.Print()
}