package main

import (
	"fmt"
	"sync"
	"strconv"
	"strings"
)

const (
	DELIMETER = ","
)

// Результат со списком всех найденных комбинаций
type Result struct {
	chains []Chain
	bestChain Chain
}

// Цепочка подстрок
type Chain struct {
	// Список идентификаторов подстрок присутствующих в данной цепочке
	ids []int

	// Текстовое представление идентификаторов подстрок
	idsStr string

	// Текст получившийся в результате склейки имеющихся подстрок
	text string

	// Курсор текущей позиции относительно исходного текста
	position int

	// Флаг указывающий на сбой в формировании цепочки подстрок
	continuity bool

	// Исходный текст
	str Str
}

// Метод для подбора комбинаций подстрок
// Этапы формирования цепочки:
// 1. Перебираем массив имеющихся сгруппированных(по количеству символов) и отсортированныз по убыванию подстрок
// 2. Если исходная строка начинается с подстроки, то формируем новую цепочку и запускаем цикл для завершения цепочки
// 3. В цикле перебирается тот же сгруппированный и отсортированный список подстрок что и в п.1, в случае сбоя
// в формаировании цепочки, текущая цепочка сбрасывается и начинается формирование новой цепочки с п.1
func (r *Result) combine(sl SubstrList, str Str) {
	chain := Chain{
		continuity: true,
		str: str,
	}

	sg := sync.WaitGroup{}

	for _, length := range sl.sort {
		for _, substr := range sl.list[length] {
			if chain.Add(substr) {
				for chain.continuity {
					sg.Add(1)
					go func() {
						for _, lgth := range sl.sort {
							for _, sbstr := range sl.list[lgth] {
								if chain.Add(sbstr) {
									sg.Done()
									return
								}
							}
						}
						chain.continuity = false
						sg.Done()
					}()

					sg.Wait()
				}
			}

			if chain.Check() {
				r.Add(chain)
			}

			chain.Reset()
		}
	}
}

// Метод для добавления готовой цепочки в список результатов
func (r *Result) Add(chain Chain) {
	ch := Chain{
		idsStr: chain.idsStr,
		ids: chain.ids,
		text: chain.text,
	}

	r.chains = append(r.chains, ch)
}

// Метод для добавления подстроки в цепочки
// Подстрока не будет добавлена в цепочку в случае не соответствия одному из условий:
// 1. Подстрока с таким ID уже имеется в цепочке
// 2. Подстрока превышает размер количества оставшися символов
// 3. Подстрока не совпадает с частью исходной строки
func (c *Chain) Add(substr Substr) bool {
	for _, id := range c.ids {
		if id == substr.id {
			return false
		}
	}

	newPosition := c.position + len(substr.text)

	if newPosition > len(c.str.text) {
		return false
	}

	if substr.text != c.str.text[c.position:newPosition] {
		return false
	}

	c.text = c.text + substr.text
	c.ids = append(c.ids, substr.id)
	c.idsStr = c.idsStr + DELIMETER + strconv.Itoa(substr.id)
	c.position = newPosition

	return true
}

// Метод для сброса состояния текущей цепочки подстрок
func (c *Chain) Reset() {
	c.ids = c.ids[0:0]
	c.text = ""
	c.position = 0
	c.continuity = true
}

// Метод "финальной" проверки соответстия составленной(из подстрок) строки с исходной строкой
func (c *Chain) Check() bool {
	if c.position == len(c.str.text) {
		if c.Text() == c.str.text {
			return true
		}
	}

	return false
}

// Метод для получения составленного(из подстрок) текста
func (c *Chain) Text() string{
	return c.text
}

// Метод для получения текстового представления идентификаторов подстрок
func (c *Chain) TextIds() string {
	return strings.Trim(c.idsStr, DELIMETER)
}

// Метод для определения лучшей цепочки подстрок
// Если лучшая цепочка не определена то будет возвращено false, инчае true
func (r *Result) Best() bool {
	least := 0
	for _, chain := range r.chains {
		if least == 0 || len(chain.ids) < least {
			r.bestChain = chain
			least = len(chain.ids)
		}
	}

	if least > 0 {
		return true
	}

	return false
}

// Метод для печати на экран лучшей цепочки подстрок
func (r *Result) Print() {
	if r.Best() == false {
		fmt.Println("Цепочка подстрок не найдена")
		return
	}

	chain := r.bestChain

	fmt.Println("Количество подстрок в цепочке: ", len(chain.ids))
	fmt.Println(chain.TextIds())
	fmt.Println("=======")
	fmt.Println(chain.Text())
}