package main

import (
	"os"
	"bufio"
	"strings"
	"strconv"
	"sort"
)

// Список подстрок
type SubstrList struct {
	// Список подстрок с группировкой по количеству символов
	list map[int][]Substr

	// DESC сортировка ключей list
	sort []int
}

// Подстрока
type Substr struct {
	// Уникальный идентификатор подстроки
	id int

	// Текст(символы) подстроки
	text string
}

// Метод для загрузки и фильтрации подстрок из файла
// Этапы работы метода:
// 1. Чтение файла подстрок по линиям(строкам)
// 2. Разбивка линий и заполнение объекта подстроки
// 3. Проверка подстроки на вхождение в исходную строку
// 4. Запись объекта подстроки в список подстрок
func (sl *SubstrList) Load(path string, str Str) {
	substrFile, err := os.Open(path)
	if err != nil {
		panic("Невозможно прочитать файл с подстроками")
	}
	defer substrFile.Close()

	sl.list = make(map[int][]Substr)

	scanner := bufio.NewScanner(substrFile)
	for scanner.Scan() {
		tmp := Substr{}
		tmp.Process(scanner.Text())
		if strings.Contains(str.text, tmp.text) {
			sl.list[len(tmp.text)] = append(sl.list[len(tmp.text)], tmp)
		}
	}
	for length := range sl.list {
		sl.sort = append(sl.sort, length)
	}

	sort.Sort(sort.Reverse(sort.IntSlice(sl.sort)))
}

// Метод для разбивки линии(строки) из файла подстрок
// Подразумевается что будет формат линии следующий:
// <id>, <chars>\n<id2>, <chars2>\n
func (s *Substr) Process(line string){
	parts := strings.Split(line, ",")
	s.id, _ = strconv.Atoi(parts[0])
	s.text = strings.Trim(parts[1], " ")
}
